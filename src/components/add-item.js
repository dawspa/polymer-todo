import {LitElement, html} from '@polymer/lit-element';

class AddItem extends LitElement {
  static get properties() {
    return {
      todoList: { type: Array },
      todoItem: { type: String }
    }
  }

  constructor() {
    super();
    this.todoItem = '';
  }

  inputKeypress(e) {
    if(e.keyCode == 13) {
      this.onAddItem();
    } else {
      this.todoItem = e.target.value;
    }
  }

  onAddItem() {
    if(this.todoItem.length > 0) {
    var storedTodoList = JSON.parse(localStorage.getItem('todo-list'));
    storedTodoList = storedTodoList === null ? [] : storedTodoList;

    storedTodoList.push({
      id: new Date().valueOf(),
      item: this.todoItem,
      done: false
    });

    localStorage.setItem('todo-list', JSON.stringify(storedTodoList));
    this.dispatchEvent(
      new CustomEvent('addItem', {
        bubbles: true,
        composed: true,
        detail: { todoList: storedTodoList }
      })
    );

    this.todoItem = '';
  }}


  render() {
    return html`
    <link rel="stylesheet" type="text/css" href="/src/styles/add-item.css">
    <div class="container">
      <div class="add">
        <div class="header">
          <h1>Add to-do</h1>
        </div>
        <div class="input-container">
          <input .value="${this.todoItem}"
          @keyup="${(e) => this.inputKeypress(e)}">
          </input>
          <button class="btn-enter" @click="${() => this.onAddItem()}">Add Item</button>
        </div>
      </div>
    </div>
    `;
  }
}

customElements.define('add-item', AddItem);
