import {LitElement, html} from '@polymer/lit-element'
import './todo-item';

class ListItems extends LitElement {
  static get properties() {
    return {
      todoList: {type: Array}
    }
  }

  constructor() {
    super();
    this.todoList = [];
  }

  render() {
    return html`
    <link rel="stylesheet" type="text/css" href="/src/styles/list-items.css">
    <div class="lists">
      <div class="list">
        <h2 class="title">Remember about...</h2>
        <div class="list-wrapper">
          ${this.todoList.map((todo) => html`<todo-item .todoItem="${todo}"></todo-item>`)}
        </div>
      </div>
    </div>
    `;
  }
}

customElements.define('list-items', ListItems);
