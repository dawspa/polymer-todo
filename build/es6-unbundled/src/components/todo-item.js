define(["../../node_modules/@polymer/lit-element/lit-element.js"],function(_litElement){"use strict";class TodoItem extends _litElement.LitElement{static get properties(){return{todoItem:{type:Object}}}constructor(){super();this.todoItem={}}onRemove(){this.dispatchEvent(new CustomEvent("removeItem",{bubbles:!0,composed:!0,detail:{itemId:this.todoItem.id}}))}onDone(){this.dispatchEvent(new CustomEvent("changeItem",{bubbles:!0,composed:!0,detail:{itemId:this.todoItem.id}}));this.requestRender()}render(){return _litElement.html`
    <link rel="stylesheet" type="text/css" href="/src/styles/todo-item.css">
    <div class="list-item">
      <input type="checkbox" .checked="${this.todoItem.done}" @click="${()=>this.onDone(this.todoItem.id)}"/>
      <div class="item">${this.todoItem.item}</div>
      <button class="delete" @click="${()=>this.onRemove(this.todoItem.id)}">
        <strong>X</strong>
      </button>
    </div>
    `}}customElements.define("todo-item",TodoItem)});