define(["../../node_modules/@polymer/lit-element/lit-element.js","./todo-item.js"],function(_litElement,_todoItem){"use strict";class ListItems extends _litElement.LitElement{static get properties(){return{todoList:{type:Array}}}constructor(){super();this.todoList=[]}render(){return _litElement.html`
    <link rel="stylesheet" type="text/css" href="/src/styles/list-items.css">
    <div class="lists">
      <div class="list">
        <h2 class="title">Remember about...</h2>
        <div class="list-wrapper">
          ${this.todoList.map(todo=>_litElement.html`<todo-item .todoItem="${todo}"></todo-item>`)}
        </div>
      </div>
    </div>
    `}}customElements.define("list-items",ListItems)});