define(["../../node_modules/@polymer/lit-element/lit-element.js"],function(_litElement){"use strict";class AddItem extends _litElement.LitElement{static get properties(){return{todoList:{type:Array},todoItem:{type:String}}}constructor(){super();this.todoItem=""}inputKeypress(e){if(13==e.keyCode){this.onAddItem()}else{this.todoItem=e.target.value}}onAddItem(){if(0<this.todoItem.length){var storedTodoList=JSON.parse(localStorage.getItem("todo-list"));storedTodoList=null===storedTodoList?[]:storedTodoList;storedTodoList.push({id:new Date().valueOf(),item:this.todoItem,done:!1});localStorage.setItem("todo-list",JSON.stringify(storedTodoList));this.dispatchEvent(new CustomEvent("addItem",{bubbles:!0,composed:!0,detail:{todoList:storedTodoList}}));this.todoItem=""}}render(){return _litElement.html`
    <link rel="stylesheet" type="text/css" href="/src/styles/add-item.css">
    <div class="container">
      <div class="add">
        <div class="header">
          <h1>Add to-do</h1>
        </div>
        <div class="input-container">
          <input .value="${this.todoItem}"
          @keyup="${e=>this.inputKeypress(e)}">
          </input>
          <button class="btn-enter" @click="${()=>this.onAddItem()}">Add Item</button>
        </div>
      </div>
    </div>
    `}}customElements.define("add-item",AddItem)});