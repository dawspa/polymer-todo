# Polymer To-Do app

A simple To-Do memo app created with _Polymer 3 LitElement_

![Polymer app screenshot](/src/polymer_screen.png)

## Access

https://polymer-todo-ds.netlify.com

To deploy locally run in terminal:

``git clone git@gitlab.com:dawspa/polymer-todo.git``

``cd polymer-todo``

``npm install``

``polymer serve --open``

## Usage

You can create an to-do item after input with both ``Add Item`` button and by pressing ``Enter``

By clicking the checbox next to item You can cross out items that are done.

Items can be deleted by clicking ``X``

This app uses local storage to keep the items, so they won't be lost as far as You won't clear Your browsers cache.

## License

Standard MIT License

**Thank You for viewing my project!**
